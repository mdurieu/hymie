using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;

public class EndgameController : MonoBehaviour
{
    public TMP_Text endText;
    GameState gameState;
    public GameObject newGameButton, newCaseButton;
    public SpriteRenderer portraitRenderer;
    Strings strings;

    string villain;
    string warrantTarget;
    string endGameString;

    private void Start()
    {
        strings = Global.instance.strings;
        portraitRenderer.sprite = Global.instance.villainPortraitCache.portrait;
        Global.instance.musicController.CacheOnce("podBay");
        Global.instance.podExteriorsCache.CacheSprite("podBay");

        gameState = Global.instance.gameState;
        Debug.Log("Villain is " + Global.instance.gameState.villain.name + "Warrant is for " + Global.instance.gameState.warrantTarget);

        SetEndgameState();    
    }

    void SetEndgameState()
    {
        villain = gameState.villain.name;
        warrantTarget = gameState.warrantTarget;

        newGameButton.SetActive(false);
        newCaseButton.SetActive(true);

        //Evaluate if case is a victory
        if (warrantTarget.Equals(villain) && gameState.timeRemaining >= 0)
        {
            gameState.victories++;
            if (gameState.victories == gameState.gameLength)
            {
                Global.instance.musicController.PlayTrack("victoryFinal", false);
                ShowEndgameSuccess();
                return;
            }
            else
            {
                endText.text = string.Format(strings.GetString("standardVictory"), villain, Global.instance.gameState.stolenObject.item);
                Global.instance.musicController.PlayTrack("victorySmall", false);
                return;
            }
        }

        //Evaluate all different failure cases
        PlayRandomFailureSong();
        gameState.failures++;



        endGameString = "";

        //Set the specific text for the circumstances of the failure
        if (gameState.currentNode.nodeName.Equals("podBay")) //Triggers if you return to the pod bay
        {
            endGameString += string.Format(strings.GetString("rtb"), villain);
        }
        else if (gameState.timeRemaining < 0)
        {
            endGameString += string.Format(strings.GetString("outOfTime"), villain);
        }
        else if (warrantTarget.Length == 0)
        {
            endGameString += string.Format(strings.GetString("noWarrant"), villain);
        }
        else if (!warrantTarget.Equals(villain))
        {
            endGameString += string.Format(strings.GetString("incorrectWarrant"), warrantTarget, villain); 
        }

        //Detect if this failure ends the entire game
        if (gameState.failures > gameState.allowedFailures || gameState.villain.name == "Hymie Lipschitz")
        {
            ShowEndgameFailure();
            return;
        }

        endText.text = endGameString;
    }


    void PlayRandomFailureSong()
    {
        int randInt = Random.Range(0, 2);
        if(randInt % 2 == 0)
        {
            Global.instance.musicController.PlayTrack("death1", false);
        } else
        {
            Global.instance.musicController.PlayTrack("death2", false);
        }
    }

    void ShowEndgameFailure()
    {
        newGameButton.SetActive(true);
        newCaseButton.SetActive(false);

        endGameString += strings.GetString("failEndGame");

        endText.text = endGameString;
    }

    void ShowEndgameSuccess()
    {
        newGameButton.SetActive(true);
        newCaseButton.SetActive(false);
        endText.text = strings.GetString("victoryEndGame");
    }

    public void OnClick(string param)
    {
        switch (param)
        {
            case "newCase":
                NewCase();
                break;

            case "newGame":
                NewGame();
                break;
        }
    }

    void NewGame()
    {
        gameState.failures = 0;
        gameState.victories = 0;
        Global.instance.gameState.currentNode = null;       
        SceneManager.LoadScene("Main");
    }

    void NewCase()
    {
        Global.instance.gameState.currentNode = null;
        SceneManager.LoadScene("Main");
    }
}
