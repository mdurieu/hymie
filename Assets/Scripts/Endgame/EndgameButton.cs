using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndgameButton : MonoBehaviour
{
    public EndgameController endgameController;
    public string newGameType; //Declare in Unity for either a new case, or reset whole game

    private void OnMouseDown()
    {
        endgameController.OnClick(newGameType);
    }
}
