using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class VillainPortraitCache : MonoBehaviour
{
    public Sprite portrait;
    public void CachePortrait()
    {
        try
        {
            Addressables.LoadAssetAsync<Sprite>(Global.instance.gameState.villain.referenceName).Completed += OnLoadDone;
        }
        catch (System.Exception)
        {

        }
    }

    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<Sprite> obj)
    {
        Sprite sprite = obj.Result;

        if (sprite == null) return;

        portrait = sprite;
    }
}
