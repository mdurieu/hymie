using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroController : MonoBehaviour
{
    AudioClip song;
    AudioSource source;

    private void Awake()
    {
        source = GetComponent<AudioSource>();

        source.loop = true;
        source.volume = 0.5f;
        source.Play();
    }
}
