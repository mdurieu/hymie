using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToPod : MonoBehaviour
{
    private void OnMouseDown()
    {
        Global.instance.gameState.loadTerminalState = "witness";
        SceneManager.LoadScene("Main");
    }
}
