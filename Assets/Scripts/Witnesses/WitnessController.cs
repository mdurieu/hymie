using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AddressableAssets;

public class WitnessController : MonoBehaviour
{
    GameState gameState;

    public Animator portraitAnimator;
    public TMP_Text hintText, witnessName;

    public SpriteRenderer backgroundRenderer;  

    float animationTimer = 0;
    void Start()
    {
        portraitAnimator.speed = 1;
        gameState = Global.instance.gameState;
        witnessName.text = Global.instance.locations.GetWitnessDisplayName(gameState.selectedWitness);

        SetHintText();

        int animationIndex = Global.instance.locations.GetWitnessAnimationIndex(gameState.selectedWitness);

        portraitAnimator.SetInteger("witness", animationIndex);
           
        SetBackground();
    }

    private void Update()
    {
        animationTimer += Time.deltaTime;
        if (animationTimer > Constants.witnessAnimationDuration) portraitAnimator.speed = 0;
    }
       

    void SetBackground()
    {
        backgroundRenderer.sprite = Global.instance.witnessBackgroundCache.GetBackground(gameState.selectedWitness);    
    }


    void SetHintText()
    {
        string nextLocation = Global.instance.gameState.currentNode.nextCorrectNode;

        string hint;

        //We're at the final node of the game where the villain is.
        if(gameState.currentNode.isVictoryNode)
        {
            hint = Global.instance.strings.GetString("beCareful");
        } else if (!gameState.currentNode.isSuspectNode)
        {
            hint = Global.instance.strings.GetString("haventSeen");
        } else
        {
            string identityHint = Global.instance.villains.GetHint(gameState.selectedWitness);
            string locationHint = Global.instance.locations.GetHint(nextLocation, gameState.selectedWitness);

            hint = identityHint + " " + locationHint;
        }

        hintText.text = hint;
    }
}
