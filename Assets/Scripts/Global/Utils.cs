using System.Collections;
using System.Collections.Generic;

public static class Utils
{
    public static string UpperFirst(string input)
    {
        string result = char.ToUpper(input[0]) + input.Substring(1);
        return result;
    }
}
