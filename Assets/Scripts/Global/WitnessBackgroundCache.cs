using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using static Locations;

public class WitnessBackgroundCache : MonoBehaviour
{
    Dictionary<string, Sprite> cachedSprites = new Dictionary<string, Sprite>();
    Dictionary<string, Location> locations;

    private void Start()
    {
        locations = Global.instance.locations.GetLocations();
    }

    public void CacheWitnesses(string location)
    {
        if (locations[location].witnesses == null) return;

        cachedSprites.Clear();
        foreach(var witness in locations[location].witnesses)
        {
            try
            {
                Addressables.LoadAssetAsync<Sprite>(witness.referenceName + "Background").Completed += OnLoadDone;
            }
            catch (System.Exception)
            {

            }
        }
    }

    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<Sprite> obj)
    {
        Sprite sprite = obj.Result;

        if (sprite == null) return;

        cachedSprites.Add(sprite.name, sprite);
    }

    public Sprite GetBackground(string witnessName)
    {
        if (!cachedSprites.ContainsKey(witnessName + "Background")) return null;

        return cachedSprites[witnessName + "Background"];
    }

}
