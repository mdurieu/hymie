using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Strings : MonoBehaviour
{
    Dictionary<string, string> stringDict;


    private void Awake()
    {
        LoadStrings();
    }

    void LoadStrings()
    {
        stringDict = new Dictionary<string, string>();

        var assets = Resources.LoadAll<TextAsset>("Strings");
        foreach(var asset in assets)
        {
            StringArray tempArray = JsonUtility.FromJson<StringArray>(asset.text);
            for (int i = 0; i < tempArray.stringPairs.Length; i++)
            {
                try
                {
                    stringDict.Add(tempArray.stringPairs[i].key, tempArray.stringPairs[i].value);
                }
                catch
                {
                    Debug.LogError("Error adding duplicate string to dictionary: " + tempArray.stringPairs[i].key);
                }                
            }
        }
    }

    public string GetString(string key)
    {
        if (stringDict.ContainsKey(key))
        {
            return stringDict[key];
        }

        return "String Resource Not Found";
    }

    public string GetCompoundString(string key)
    {
        //Just try/catch this whole thing due to the multiple dictionary references
        try
        {
            switch (key)
            {
                case "shortDuration":
                    return string.Format(stringDict["shortGame"], Constants.shortGameLength);

                case "mediumDuration":
                    return string.Format(stringDict["mediumGame"], Constants.medGameLength);

                case "longDuration":
                    return string.Format(stringDict["longGame"], Constants.longGameLength);
            }
        }
        catch{}

        return "String Resource Not Found";
    }


    [System.Serializable]
    public class StringPair
    {
        public string key, value;
    }

    [System.Serializable]
    public class StringArray
    {
        public StringPair[] stringPairs;
    }

}
