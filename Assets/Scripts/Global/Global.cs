using UnityEngine;
using UnityEngine.SceneManagement;


/* This class is attached to a prefab and instantiated on launch of the main scene.
 * It contains references to persistent classes we want to keep active. It is the 
 * only singleton in the game*/

public class Global : MonoBehaviour
{
    public static Global instance;

    public Strings strings;
    public GameState gameState;
    public StolenObjects stolenObjects;
    public Locations locations;
    public GameController gameController;
    public Villains villains;
    public MusicController musicController;
    public SoundController soundController;
    public WitnessBackgroundCache witnessBackgroundCache;
    public PodExteriorsCache podExteriorsCache;
    public VillainPortraitCache villainPortraitCache;

    void Awake()
    {
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 30;

        Screen.fullScreen = false;

        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }         
    }

    
}
