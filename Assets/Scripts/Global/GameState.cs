using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Locations;
using static StolenObjects;

public class GameState : MonoBehaviour
{
    public StolenObject stolenObject;
    public Villain villain;
    public Dictionary<string, int[]> timeCodes;
    public List<TravelNode> travelNodes;
    public TravelNode currentNode;

    public int victories = 0;
    public int failures = 0;

    public int timeRemaining = 0;

    public int gameLength;
    public int allowedFailures;

    public Dictionary<string, int> selectedWarrantTraits;
    public Dictionary<string, string[]> warrantTraitDefinitions;
    public string warrantTarget = "";

    public string selectedWitness;
    public string loadTerminalState = ""; //Set this variable to tell the terminal what state to initialise into when loading the pod view (like when returning from a witness)

    public List<int> timeCode;  //TODO refactor this as a stack - not really necessary but good exercise

    //Randomise the order button locations appear, but store them so they appear consistently
    public Dictionary<string, List<string>> randomisedTravelLocations = new Dictionary<string, List<string>>();

    private void Awake()
    {
        GenerateTimeCodes();     
    }

    public void GenerateGame()
    {
        timeRemaining = (Constants.nodesInCrimeTrail + Constants.extraJumpsAllowed) * Constants.timeTravelCost + ((Constants.nodesInCrimeTrail * 3) + Constants.extraWitnessVisitsAllowed) * Constants.witnessVisitCost;

        timeCode = new List<int>();

        InitialiseWarrant();

        stolenObject = Global.instance.stolenObjects.GetRandomObject();
        Debug.Log("Stolen object is " + stolenObject.item + ", " + stolenObject.location);       

        travelNodes = GeneratePath();

        RandomiseTravelLocations();

        Global.instance.gameController.SetVictoryWitnesses(travelNodes);

        villain = Global.instance.villains.GetVillain();
        Global.instance.villainPortraitCache.CachePortrait();

        Debug.Log("Villain is " + villain.name);

        Global.instance.villains.GenerateHints(villain, travelNodes);
        Global.instance.locations.GenerateHints(System.Array.Find(villain.traits, element => element.name.Equals("sex")).value, travelNodes);

        travelNodes.Add(new TravelNode(false, "ortega"));
        travelNodes.Add(new TravelNode(false, "roomRemovedForLegalReasons"));
    }

    void InitialiseWarrant()
    {
        warrantTarget = "";
        warrantTraitDefinitions = Global.instance.villains.GetTraits();  //This returns a clone, not a reference so it's safe to manipulate

        selectedWarrantTraits = new Dictionary<string, int>();

        //Initialise all the traits as selection 0
        foreach (var trait in warrantTraitDefinitions)
        {
            selectedWarrantTraits[trait.Key] = 0;
        }        

        //Can't modify traitDefinitions dictionary above while iterating over it
        //Add "---" as a "no selection' value at the start of each traitDefinition
        foreach (var trait in selectedWarrantTraits)
        {
            warrantTraitDefinitions[trait.Key] = new string[] { "---" }.Concat(warrantTraitDefinitions[trait.Key]).ToArray();
        }
    }

    List<TravelNode> GeneratePath()
    {
        var newNodes = new List<TravelNode>();

        //Add the location of the theft as the first location
        newNodes.Add(new TravelNode(false, "podBay"));
        newNodes[0].nextCorrectNode = stolenObject.location;

        currentNode = newNodes[0];

        //Keep track of the locations used in the main path
        List<string> usedLocations = new List<string>();
        usedLocations.Add(stolenObject.location);

        Dictionary<string, Location> locations = Global.instance.locations.GetLocations();

        //Fill out the main path of nodes which follow the criminal
        for (int i = 0; i < Constants.nodesInCrimeTrail - 1; i++)
        {            
            string nextLocation = "";
            do
            {
                nextLocation = locations.ElementAt(Random.Range(0, locations.Count)).Value.referenceName;
            } while (usedLocations.Contains(nextLocation) || Global.instance.locations.GetLocation(nextLocation).witnesses == null);

            usedLocations.Add(nextLocation);

            TravelNode newNode = new TravelNode(true, newNodes[i].nextCorrectNode);
            newNode.nextCorrectNode = nextLocation;
            if (newNodes[i].nodeName != "podBay") newNode.previousCorrectNode = newNodes[i].nodeName;

            newNodes.Add(newNode);
        }

        //Add in the final node
        TravelNode finalNode = new TravelNode(true, newNodes[newNodes.Count - 1].nextCorrectNode);
        finalNode.isVictoryNode = true;
        finalNode.previousCorrectNode = newNodes[newNodes.Count - 1].nodeName;
        newNodes.Add(finalNode);

        //This is just for dev
        string path = "";
        foreach(var node in newNodes)
        {
            path += node.nodeName + " -->";
        }

        Debug.Log(path);

        //Add extra locations that aren't part of the crime trail as decoys
        //Make a list of valid decoy locations (don't modify locations var as it is a reference)
        List<string> validDecoyLocations = new List<string>();
        foreach(var location in locations)
        {
            if (!usedLocations.Contains(location.Value.referenceName))
            {
                validDecoyLocations.Add(location.Value.referenceName);
            }
        }

        //Add decoy locations to all the intermediate points (so, not to dispatch, or the final location)
        //Add decoys into a separate list while generating just for clarity, merge lists after
        List<TravelNode> decoyNodes = new List<TravelNode>();

        //Keep track of the decoys which have been used
        List<string> usedDecoys = new List<string>();

        for(int i = 1; i < Constants.nodesInCrimeTrail; i++)
        {
            string location1, location2;
            int safety = 0;
            do
            {                
                location1 = validDecoyLocations.ElementAt(Random.Range(0, validDecoyLocations.Count));
                location2 = validDecoyLocations.ElementAt(Random.Range(0, validDecoyLocations.Count));

                //If the number of locations isn't enough to make a main path and fill it with decoys, this loop needs to fail
                //This is really just here so that it doesn't crash unity if you make a mistake while developing
                if (safety++ > 100000)
                {
                    Debug.Log("hit safety");
                    Application.Quit();
                    break;
                }
            } while (location1.Equals(location2) || Global.instance.locations.GetLocation(location1).witnesses == null || Global.instance.locations.GetLocation(location2).witnesses == null);

            //Remove chosen locations so they don't repeat
            validDecoyLocations.Remove(location1);
            validDecoyLocations.Remove(location2);

            usedDecoys.Add(location1);
            usedDecoys.Add(location2);

            newNodes[i].decoyNodes.Add(location1);
            newNodes[i].decoyNodes.Add(location2);

            TravelNode decoy1 = new TravelNode(false, location1);
            decoy1.previousCorrectNode = newNodes[i].nodeName;

            TravelNode decoy2 = new TravelNode(false, location2);
            decoy2.previousCorrectNode = newNodes[i].nodeName;

            decoyNodes.Add(decoy1);
            decoyNodes.Add(decoy2);
        }

        //Once all the decoys have been assigned, link them to other random decoys, too. This prevents there being only one option when at a decoy
        //node and thus giving away that it is a decoy.
        //It is possible the player could through sheer luck shortcut to the end, but they will miss most of the warrant info. This edge case is ok.

        foreach(TravelNode decoy in decoyNodes)
        {
            TravelNode decoy1, decoy2, decoy3;

            do
            {
                decoy1 = decoyNodes.ElementAt(Random.Range(0, decoyNodes.Count));
                decoy2 = decoyNodes.ElementAt(Random.Range(0, decoyNodes.Count));
                decoy3 = decoyNodes.ElementAt(Random.Range(0, decoyNodes.Count));

            } while (decoy1.nodeName.Equals(decoy.nodeName) || decoy2.nodeName.Equals(decoy.nodeName) || decoy3.nodeName.Equals(decoy.nodeName)
                  || decoy1.nodeName.Equals(decoy2.nodeName) || decoy1.nodeName.Equals(decoy3.nodeName) || decoy2.nodeName.Equals(decoy3.nodeName));

            decoy.decoyNodes.Add(decoy1.nodeName);
            decoy.decoyNodes.Add(decoy2.nodeName);
            decoy.decoyNodes.Add(decoy3.nodeName);
        }

        newNodes.AddRange(decoyNodes);

        return newNodes;
    }

    void RandomiseTravelLocations()
    {
        foreach(TravelNode node in travelNodes)
        {
            List<string> locations = new List<string>();
            if (node.previousCorrectNode.Length > 0) locations.Add(node.previousCorrectNode);
            if (node.nextCorrectNode.Length > 0) locations.Add(node.nextCorrectNode);

            foreach (string decoy in node.decoyNodes)
            {
                locations.Add(decoy);
            }

            var shuffledLocations = locations.OrderBy(a => Random.Range(0f, 1f)).ToList();

            randomisedTravelLocations[node.nodeName] = shuffledLocations;
        }       
    }

    void GenerateTimeCodes()
    {
        timeCodes = new Dictionary<string, int[]>();

        timeCodes.Add("ortega", new int[] { 0, 1, 2, 3, 4, 5 });
        timeCodes.Add("roomRemovedForLegalReasons", new int[] { 0, 0, 0, 0, 0, 6 });

        foreach (var entry in Global.instance.locations.GetLocations())
        {
            if (timeCodes.ContainsKey(entry.Value.referenceName)) continue;
            timeCodes.Add(entry.Value.referenceName, GetUniqueTimeCode());
        }      
    }

    int[] GetUniqueTimeCode()
    {
        int[] code;
        do
        {
            code = new int[Constants.timeCodeLength];

            for (int i = 0; i < code.Length; i++)
            {
                code[i] = Random.Range(0, Constants.timecodeCharCount);
            }

        } while (!codeIsUnique(code));

        return code;
    }

    bool codeIsUnique(int[] code)
    {
        foreach(var existing in timeCodes)
        {
            if (code.SequenceEqual(existing.Value)) return false;
        }
        return true;
    }
}

public class TravelNode
{
    public List<string> decoyNodes = new List<string>();
    public string nextCorrectNode = "", previousCorrectNode = "";
    public bool isSuspectNode;
    public bool isVictoryNode;
    public string nodeName;

    public TravelNode(bool isSuspectNode, string nodeName) {
        this.isSuspectNode = isSuspectNode;
        this.nodeName = nodeName;
    }
}
