using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Locations : MonoBehaviour
{
    Dictionary<string, Location> locations;
    string[] attributes;

    Dictionary<string, List<string>> hints; //Key is the location the hint is for, Value is the list of hints
    Dictionary<string, string> allocatedHints; //Key is the witness name, Value is their hint

    private void Awake()
    {
        LoadLocations();
    }

    //These are the hints that are used to describe this location from the previous one in the trail
    public void GenerateHints(string villainPronoun, List<TravelNode> travelNodes)
    {
        List<string> hintStarters = new List<string>();

        hintStarters.Add(char.ToUpper(villainPronoun[0]) + villainPronoun.Substring(1) + Global.instance.strings.GetString("travelling"));
        hintStarters.Add(char.ToUpper(villainPronoun[0]) + villainPronoun.Substring(1) + Global.instance.strings.GetString("visit"));

        hints = new Dictionary<string, List<string>>();
        allocatedHints = new Dictionary<string, string>();

        foreach (TravelNode node in travelNodes)
        {
            if (!node.isSuspectNode) continue; //Only suspect nodes need hints about them as locations

            hints.Add(node.nodeName, new List<string>());
           
            foreach (int attribute in locations[node.nodeName].attributes)
            {
                hints[node.nodeName].Add(hintStarters[Random.Range(0, hintStarters.Count)] + attributes[attribute] + ".");
            }
        }
    }

    public string GetHint(string location, string witness)
    {
        if (!allocatedHints.ContainsKey(witness))
        {
            int hintIndex = Random.Range(0, hints[location].Count);

            allocatedHints.Add(witness, hints[location][hintIndex]);
            hints[location].RemoveAt(hintIndex);
        }

        return allocatedHints[witness];     
    }

    void LoadLocations()
    {
        locations = new Dictionary<string, Location>();

        string filePath = "Locations/Locations";

        TextAsset asset = Resources.Load<TextAsset>(filePath);

        LocationDefinitions definitions = JsonUtility.FromJson<LocationDefinitions>(asset.text);

        attributes = definitions.allAttributes;

        for (int i = 0; i < definitions.locations.Length; i++)
        {
            locations.Add(definitions.locations[i].referenceName, definitions.locations[i]);
        }
    }

    public string GetAttribute(int index)
    {
        return attributes[index];
    }

    public string GetLocationDisplayName(string referenceName)
    {
        foreach (var entry in locations)
        {            
           if (entry.Key == referenceName) return entry.Value.displayName;            
        }

        return "Display name not found " + referenceName;
    }

    public string GetWitnessDisplayName(string name)
    {
        foreach (var entry in locations)
        {
            foreach (var witness in entry.Value.witnesses)
            {
                if (witness.referenceName == name) return witness.displayName;
            }
        }

        return "Display name not found " + name;
    }

    public int GetWitnessAnimationIndex(string name)
    {
        foreach(var entry in locations)
        {
            foreach(var witness in entry.Value.witnesses)
            {
                if (witness.referenceName == name) return witness.animationIndex;
            }
        }

        return -1;
    }

    public Location GetLocation(string name)
    {
        return locations[name];
    }

    public Dictionary<string, Location> GetLocations()
    {
        return locations;
    }

    [System.Serializable]
    public class Location
    {
        public string displayName, referenceName;
        public Witness[] witnesses;
        public int[] attributes;
    }

    [System.Serializable]
    public class Witness
    {
        public string displayName, referenceName;
        public int animationIndex;
    }

    [System.Serializable]
    public class LocationDefinitions
    {
        public Location[] locations;
        public string[] allAttributes;
    }
}
