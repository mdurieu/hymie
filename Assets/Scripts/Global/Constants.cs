using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static int shortGameLength = 3;
    public static int medGameLength = 5;
    public static int longGameLength = 10;

    public static int shortGameFailures = 1;
    public static int medGameFailures = 2;
    public static int longGameFailures = 3;

    public static int timecodeCharCount = 15;
    public static int timeCodeLength = 6;

    public static int nodesInCrimeTrail = 4;

    public static Color textColor = new Color(0, 1, 0);
    public static Color textColorDark = new Color(0, 0.33f, 0);

    public static float witnessAnimationDuration = 5f;

    public static float timeTravelDuration = 4f;

    public static int timeTravelCost = 2;
    public static int witnessVisitCost = 1;
    public static int extraJumpsAllowed = 2; //One to the wrong place, then one back
    public static int extraWitnessVisitsAllowed = 2;
}
