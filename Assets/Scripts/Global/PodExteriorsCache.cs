using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class PodExteriorsCache : MonoBehaviour
{
    public Sprite cachedSprite;

    public void CacheSprite(string location)
    {
        try
        {
            Addressables.LoadAssetAsync<Sprite>(location).Completed += OnLoadDone;
        }
        catch (System.Exception)
        {

        }
    }

    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<Sprite> obj)
    {
        Sprite sprite = obj.Result;

        if (sprite == null) return;

        cachedSprite = sprite;
    }
}
