using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StolenObjects : MonoBehaviour
{
    List<StolenObject> objects;


    private void Awake()
    {
        LoadObjects();
    }

    void LoadObjects()
    {
        objects = new List<StolenObject>();

        string filePath = "StolenObjects/StolenObjects";

        TextAsset asset = Resources.Load<TextAsset>(filePath);

        StringArray tempArray = JsonUtility.FromJson<StringArray>(asset.text);

        for (int i = 0; i < tempArray.stolenObjects.Length; i++)
        {
            objects.Add(tempArray.stolenObjects[i]);
        }
    }


    public StolenObject GetRandomObject()
    {
        return objects[Random.Range(0, objects.Count)];          
    }

    [System.Serializable]
    public class StolenObject
    {
        public string item, location;
    }

    [System.Serializable]
    public class StringArray
    {
        public StolenObject[] stolenObjects;
    }

}