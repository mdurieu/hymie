using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class SoundController : MonoBehaviour
{
    AudioSource source;

    Dictionary<string, AudioClip> cache = new Dictionary<string, AudioClip>();
    Dictionary<string, string> assetNames = new Dictionary<string, string>();

    private void Awake()
    {
        source = GetComponent<AudioSource>();

        source.volume = 0.7f;

        CacheSound("buttonPress");
        CacheSound("negativeChime");
        CacheSound("positiveChime");
    }

    IEnumerator GetResourceId(string referenceName)
    {
        //Returns any IResourceLocations that are mapped to the key "referenceName"
        AsyncOperationHandle<IList<IResourceLocation>> handle = Addressables.LoadResourceLocationsAsync(referenceName);
        yield return handle;

        var res = handle.Result;

        foreach (var r in res)
        {
            //Strip out the path and file extension, this will match audioclip.name once the resource is loaded
            string strippedPath = r.InternalId.Substring(r.InternalId.IndexOf("Sounds/") + 7);
            string trackName = strippedPath.Substring(0, strippedPath.Length - 4);

            assetNames.Add(trackName, referenceName);
        }

        Addressables.Release(handle);

        Addressables.LoadAssetAsync<AudioClip>(referenceName).Completed += OnCacheLoadDone;
    }

    void CacheSound(string name)
    {
        StartCoroutine(GetResourceId(name));
    }

    void OnCacheLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<AudioClip> obj)
    {
        AudioClip clip = obj.Result;

        if (clip == null) return;

        cache.Add(assetNames[obj.Result.name], clip);
    }

    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<AudioClip> obj)
    {
        AudioClip clip = obj.Result;

        if (clip == null) return;

        source.PlayOneShot(clip);
    }

    public void PlayOnce(string name)
    {
        if (cache.ContainsKey(name))
        {
            source.PlayOneShot(cache[name]);
            return;
        }

        Addressables.LoadAssetAsync<AudioClip>(name).Completed += OnLoadDone;
    }
}
