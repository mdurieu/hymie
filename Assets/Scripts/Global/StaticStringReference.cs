using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StaticStringReference : MonoBehaviour
{
    public string stringReference;
    public bool isCompoundString;

    private void Start()
    {
        TMP_Text TMPText = GetComponent<TMP_Text>();
        TMPText.text = isCompoundString ? Global.instance.strings.GetCompoundString(stringReference) : Global.instance.strings.GetString(stringReference);
    }
}
