using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using static Locations;

public class Villains : MonoBehaviour
{
    Dictionary<string, string[]> traitDefinitions;

    Dictionary<string, Villain> villains; //Key is villain name, Value is dictionary of traits and their values
    
    Dictionary<string, string> hints; //Key is the witness this hint is allocated to, Value is the hint itself

    GameState gameState;

    private void Awake()
    {
        LoadDefinitions();
        LoadVillains();
        Validate();
    }

    private void Start()
    {
        gameState = Global.instance.gameState;
    }

    public void GenerateHints(Villain villain, List<TravelNode> nodes)
    {
        List<string> tempHints = new List<string>();
        hints = new Dictionary<string, string>();

        string sex = System.Array.Find(villain.traits, element => element.name.Equals("sex")).value;      

        List<string> hintStartersHad = new List<string>();
        List<string> hintStartersWas = new List<string>();

        Strings strings = Global.instance.strings;

       // hintStartersHad.Add("I heard " + sex + " had ");
        hintStartersHad.Add(string.Format(strings.GetString("heardXHad"), sex));
        hintStartersHad.Add(char.ToUpper(sex[0]) + sex.Substring(1) + strings.GetString("had"));

        //  hintStartersWas.Add("I heard " + sex + " was ");
        hintStartersHad.Add(string.Format(strings.GetString("heardXWas"), sex));
        hintStartersWas.Add(char.ToUpper(sex[0]) + sex.Substring(1) + strings.GetString("was"));

        foreach (Trait trait in villain.traits)
        {         
            switch (trait.name)
            {
                case "species":
                    tempHints.Add(string.Format(strings.GetString("haveSeen"), trait.value));
                    break;

                case "personality":
                    tempHints.Add(hintStartersWas[Random.Range(0, hintStartersWas.Count)] + strings.GetString("rather") + trait.value + ".");
                    break;

                case "hair":
                    tempHints.Add(hintStartersHad[Random.Range(0, hintStartersWas.Count)] + trait.value + strings.GetString("hair"));
                    break;

                case "features":
                    tempHints.Add(hintStartersHad[Random.Range(0, hintStartersWas.Count)] + strings.GetString("a") + trait.value + ".");
                    break;
            }
        }

        //Get the names of all the witnessess who can possibly give hints
        List<string> witnesses = new List<string>();

        foreach(TravelNode node in nodes)
        {
            if (!node.isSuspectNode || node.isVictoryNode) continue;

            Location location = Global.instance.locations.GetLocation(node.nodeName);

            foreach(Witness witness in location.witnesses)
            {
                witnesses.Add(witness.referenceName);
            }
        }

        //Randomly allocate the hints to the witnesses so they are spread out over the game
        while(tempHints.Count > 0)
        {
            int hintIndex = Random.Range(0, tempHints.Count);
            int witnessIndex = Random.Range(0, witnesses.Count);

            hints.Add(witnesses[witnessIndex], tempHints[hintIndex]);

            tempHints.RemoveAt(hintIndex);
            witnesses.RemoveAt(witnessIndex);
        }
    }

    public string GetHint(string witness)
    {
        if (!hints.ContainsKey(witness))
        {
            return "";
        }

        return hints[witness];
    }

    public Villain GetVillain()
    {
        //Return Hymie as the villain if it's the last one of the game
        if(gameState.victories == gameState.gameLength - 1)
        {
            return villains["Hymie Lipschitz"];
        }

        //Otherwise, return a random, non-Hymie villain
        Villain villain;
        do
        {
            villain = villains.ElementAt(Random.Range(0, villains.Count)).Value;
        } while (villain.name == "Hymie Lipschitz");

        return villain;
    }

    public Dictionary<string, Villain> GetVillains()
    {
        var newDict = new Dictionary<string, Villain>(villains);
        return newDict;
    }

    public Dictionary<string, string[]> GetTraits()
    {
        var newDict = new Dictionary<string, string[]>(traitDefinitions);
        return newDict;
    }


    //This will ensure that all the traits and values given to the villains match the definitions
    void Validate()
    {
        foreach(var entry in villains)
        {
            foreach(Trait trait in entry.Value.traits)
            {
                if(!traitDefinitions.ContainsKey(trait.name))
                {
                    Debug.LogError("Unrecognised trait: " + entry.Key + ": " + trait.value);
                }
                if (!traitDefinitions[trait.name].Contains(trait.value))
                {
                    Debug.LogError("Unrecognised trait value: " + entry.Key + ": " + trait.name + ": " + trait.value);
                }
            }
        }
    }

    void LoadDefinitions()
    {
        traitDefinitions = new Dictionary<string, string[]>();

        string filePath = "Villains/VillainTraits";

        TextAsset asset = Resources.Load<TextAsset>(filePath);

        TraitsArray tempArray = JsonUtility.FromJson<TraitsArray>(asset.text);

        for (int i = 0; i < tempArray.traitDefinitions.Length; i++)
        {
            traitDefinitions.Add(tempArray.traitDefinitions[i].name, tempArray.traitDefinitions[i].values);
        }
    }

    void LoadVillains()
    {
        villains = new Dictionary<string, Villain>();

        string filePath = "Villains/Villains";

        TextAsset asset = Resources.Load<TextAsset>(filePath);

        VillainsArray tempArray = JsonUtility.FromJson<VillainsArray>(asset.text);

        for (int i = 0; i < tempArray.villains.Length; i++)
        {         
            villains.Add(tempArray.villains[i].name, tempArray.villains[i]);          
        }
    }
}


[System.Serializable]
public class TraitDefinition
{
    public string name;
    public string[] values;
}

[System.Serializable]
public class TraitsArray
{
    public TraitDefinition[] traitDefinitions;
}



[System.Serializable]
public class Villain
{
    public string name, referenceName;
    public Trait[] traits;
}

[System.Serializable]
public class Trait
{
    public string name;
    public string value;
}

[System.Serializable]
public class VillainsArray
{
    public Villain[] villains;
}