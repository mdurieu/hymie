using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Locations;

public class GameController : MonoBehaviour
{
    List<string> victoryNodeWitnessesToVisit; //Track the witnesses on the victory node so that the game can resolve once all have been visited

    float travelTimer;
    bool travelling = false;

    private void Awake()
    {
        //This is incredibly hacky!! But, the addressables API doesn't propagate exceptions properly
        //This is necessary so that the try/catches around the LoadAssetAsync calls can fail if no asset exists
        //This works application-wide for all the classes using addressables
        UnityEngine.ResourceManagement.ResourceManager.ExceptionHandler = (op, ex) => throw ex;
    }

    public void TravelTo(TravelNode destination)
    {
        //If going to ortega or legal room, set the next node as the one you just came from so you can get back (or to the first node if coming from HQ)
        if (destination.nodeName.Equals("ortega") || destination.nodeName.Equals("roomRemovedForLegalReasons"))
        {
            string travelOption = "";

            if (Global.instance.gameState.currentNode.nodeName.Equals("podBay"))
            {
                travelOption = Global.instance.gameState.travelNodes[1].nodeName;
            } else
            {
                travelOption = Global.instance.gameState.currentNode.nodeName;
            }

           Global.instance.gameState.randomisedTravelLocations[destination.nodeName] = new List<string>() { travelOption };
        }

        if ((Global.instance.gameState.timeRemaining -= Constants.timeTravelCost) < 0) //Perform the subtraction and evaluation at the same time
        {
            SceneManager.LoadScene("Endgame");
            return;
        }

        Global.instance.musicController.CacheOnce(destination.nodeName); //Cache the song for the upcoming scene while pod is in transit
        Global.instance.witnessBackgroundCache.CacheWitnesses(destination.nodeName); //Cache the backgrounds for the upcoming witnesses
        Global.instance.podExteriorsCache.CacheSprite(destination.nodeName);

        Global.instance.gameState.currentNode = destination;

        travelTimer = 0;
        travelling = true;
        GameObject.Find("TravelAnimation").GetComponent<SpriteRenderer>().enabled = true;
        Global.instance.musicController.PlayTrack("timeTravel", false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (travelling)
        {
            travelTimer += Time.deltaTime;
            if(travelTimer > Constants.timeTravelDuration)
            {
                travelling = false;
                GameObject.Find("TravelAnimation").GetComponent<SpriteRenderer>().enabled = false;
                GameObject.Find("PodExterior").GetComponent<PodExterior>().SetSprite();
                Global.instance.musicController.PlayTrack(Global.instance.gameState.currentNode.nodeName, true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    public void SetVictoryWitnesses(List<TravelNode> nodes)
    {
        victoryNodeWitnessesToVisit = new List<string>();

        foreach (var node in nodes)
        {
            if (!node.isVictoryNode) continue;

            Location location = Global.instance.locations.GetLocation(node.nodeName);

            foreach(Witness witness in location.witnesses)
            {
                victoryNodeWitnessesToVisit.Add(witness.referenceName);
            }
           
        }
    }

    public bool WitnessConcludesGame(string witnessName)
    {
        victoryNodeWitnessesToVisit.Remove(witnessName);

        if(victoryNodeWitnessesToVisit.Count == 1)
        {
            string villain = Global.instance.gameState.villain.name;
            string warrantTarget = Global.instance.gameState.warrantTarget;

            if (warrantTarget.Equals(villain))
            {
                if(villain.Equals("Hymie Lipschitz"))
                {
                    Debug.Log("Caching final victory");
                    Global.instance.musicController.CacheOnce("victoryFinal");
                } else
                {
                    Debug.Log("Caching small victory");
                    Global.instance.musicController.CacheOnce("victorySmall");
                }
            }
        }

        return victoryNodeWitnessesToVisit.Count == 0;        
    }


}
