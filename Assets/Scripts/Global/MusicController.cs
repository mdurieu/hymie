using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class MusicController : MonoBehaviour
{
    AudioSource source;

    Dictionary<string, AudioClip> cache = new Dictionary<string, AudioClip>();
    Dictionary<string, string> assetNames = new Dictionary<string, string>();

    KeyValuePair<string, AudioClip> cacheOnce;

    private void Awake()
    {
        source = GetComponent<AudioSource>();

        source.volume = 0.5f;
        CacheSong("death1");
        CacheSong("death2");
        CacheSong("timeTravel");
    }

    IEnumerator GetResourceId(string referenceName)
    {
        //Returns any IResourceLocations that are mapped to the key "referenceName"
        AsyncOperationHandle<IList<IResourceLocation>> handle = Addressables.LoadResourceLocationsAsync(referenceName);
        yield return handle;

        var res = handle.Result;

        foreach(var r in res)
        {
            //Strip out the path and file extension, this will match audioclip.name once the resource is loaded
            string strippedPath = r.InternalId.Substring(r.InternalId.IndexOf("Music/") + 6);
            string trackName = strippedPath.Substring(0, strippedPath.Length - 4);
            assetNames.Add(trackName, referenceName);
        }

        Addressables.Release(handle);

        try
        {
            Addressables.LoadAssetAsync<AudioClip>(referenceName).Completed += OnCacheLoadDone;
        }
        catch (System.Exception)
        {

        }
               
    }   

    void CacheSong(string name)
    {
        StartCoroutine(GetResourceId(name));        
    }

    public void CacheOnce(string name)
    {
        cacheOnce = new KeyValuePair<string, AudioClip>(name, null); //Set the name here so we don't have to go looking for the resource like in the main cache method
        try
        {
            Addressables.LoadAssetAsync<AudioClip>(name).Completed += OnCacheOnceLoadDone;
        }
        catch (System.Exception)
        {
            cacheOnce = default;
   
        }
        
    }

    void OnCacheOnceLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<AudioClip> obj)
    {
        AudioClip clip = obj.Result;

        if (clip == null) {
            cacheOnce = default;
            return;
        }

        cacheOnce = new KeyValuePair<string, AudioClip>(cacheOnce.Key, clip);
    }

    void OnCacheLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<AudioClip> obj)
    {
        AudioClip clip = obj.Result;

        if (clip == null) return;
        cache.Add(assetNames[obj.Result.name], clip);           
    }

    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<AudioClip> obj)
    {
        AudioClip clip = obj.Result;

        if (clip == null) return;

        source.clip = clip;
        source.Play();
    }

    public void PlayTrack(string name, bool loop)
    {
        source.Stop();
        source.loop = loop;

        if (source.clip.name == name) //This is here as the initial podbay track is set in the inspector to decrease load time on first run
        {
            source.Play();
            return;
        }        

        if(!cacheOnce.Equals(default) && cacheOnce.Key == name)
        {
            source.clip = cacheOnce.Value;
            source.Play();
            return;
        }

        if (cache.ContainsKey(name))
        {
            source.clip = cache[name];
            source.Play();
            return;
        }

        //Last resort if the song isn't in either cache, load it on the fly
        try
        {
            Addressables.LoadAssetAsync<AudioClip>(name).Completed += OnLoadDone;
        }
        catch (System.Exception)
        {

        }
    }
}
