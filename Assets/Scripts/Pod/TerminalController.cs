using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using static Locations;
using UnityEngine.SceneManagement;

public class TerminalController : MonoBehaviour
{
    public TMP_Text briefingText, briefingTextSmall, questionWitnessText, questionWitnessTextSmall, apprehensionsValue, failuresValue, apprehensionsValueSmall, failuresValueSmall, timeRemainingValue, timeRemainingValueSmall;
   //TODO refactor naming so these are all referred to as objects (as per travelStateObject)
    public GameObject newGameState, newGameStateSmall, briefingState, briefingStateSmall, mainMenuState,
        mainMenuStateSmall, travelStateObject, largeTerminal, witnessState, dossierState, warrantState;
    public List<GameObject> travelButtons, witnessButtons;
    public BoxCollider2D questionWitnessCollider, smallTerminalCollider;
    int briefingHassleCount = 0; //How many times the hassling message has appeared when accepting the briefing (ie would you like space spuds with that)
    public string state;
    public DossierPagenator dossierPagenator;
    public WarrantController warrantController;
    public TravelState travelState;
    public TimecodeController timecodeController;
    GameState gameState;



    private void Start()
    {
        gameState = Global.instance.gameState;       

        if (gameState.currentNode == null)
        {
            Global.instance.musicController.PlayTrack("podBay", true);
            gameState.currentNode = new TravelNode(false, "podBay"); //Set a contrived node here to check if the player is trying to travel to the bay at the start of the game
           

            if (gameState.victories == 0 && gameState.failures == 0) //Will be the case if it's a brand new game
            {
                SetState("newGame");
            } else
            {
                gameState.GenerateGame();
                SetState("briefing");
            }

            timecodeController.SetCodeToPodBay(); //Do this after game generation as that resets the time code

        } else 
        {
            switch (gameState.loadTerminalState)
            {
                case "witness":
                    SetState("questionWitnessState");
                    SetState("showTerminal");
                    break;
            }          
        }
    }

    public void SelectGameDuration(int duration, int failures) {
        gameState.gameLength = duration;
        gameState.allowedFailures = failures;
        gameState.GenerateGame();
        SetState("briefing");
    }

    public void SetState(string newState, string param = "")
    {
        //Set these here even though it's more often than needed, too many edge cases otherwise
        timeRemainingValue.text = gameState.timeRemaining.ToString();
        timeRemainingValueSmall.text = gameState.timeRemaining.ToString();

        switch (newState)
        {
            case "showTerminal":
                smallTerminalCollider.enabled = false;
                largeTerminal.SetActive(true);
                break;

            case "hideTerminal":
                largeTerminal.SetActive(false);
                smallTerminalCollider.enabled = true;
                mainMenuStateSmall.SetActive(true);

                //Reset to main menu if exiting from a submenu (saves work in having to make the small terminal match)                
                if (state == "travel" || state == "questionWitnessState")
                {
                    Debug.Log("Main menu set in hide terminal");
                    SetState("mainMenu");
                }

                break;

            case "newGame":
                DisableStates();
                newGameState.SetActive(true);
                newGameStateSmall.SetActive(true);
                break;

            case "briefing":
                DisableStates();
                briefingState.SetActive(true);
                briefingStateSmall.SetActive(true);

                string item = Global.instance.gameState.stolenObject.item;
                string location = Global.instance.locations.GetLocation(Global.instance.gameState.stolenObject.location).displayName;
                string text = string.Format(Global.instance.strings.GetString("briefing"), item, location);

                briefingText.text = text;
                briefingTextSmall.text = text;
                break;

            case "briefingAccepted":
                if(briefingHassleCount < 3)
                {
                    briefingText.text = Global.instance.strings.GetString("briefingHassle" + briefingHassleCount);
                    briefingTextSmall.text = Global.instance.strings.GetString("briefingHassle" + briefingHassleCount);
                    briefingHassleCount++;
                    break;
                }

                DisableStates();
                SetState("mainMenu");
                break;

            case "mainMenu":
                DisableStates();

                apprehensionsValue.text = gameState.victories.ToString();
                failuresValue.text = gameState.failures.ToString();
                apprehensionsValueSmall.text = gameState.victories.ToString();
                failuresValueSmall.text = gameState.failures.ToString();

                mainMenuState.SetActive(true);
                mainMenuStateSmall.SetActive(true);

                if(Global.instance.locations.GetLocation(Global.instance.gameState.currentNode.nodeName).witnesses == null)
                {
                    questionWitnessText.color = Constants.textColorDark;
                    questionWitnessTextSmall.color = Constants.textColorDark;
                    questionWitnessCollider.enabled = false;
                } else
                {
                    questionWitnessText.color = Constants.textColor;
                    questionWitnessTextSmall.color = Constants.textColor;
                    questionWitnessCollider.enabled = true;
                }
                break;

            case "travel":
                DisableStates();
                travelStateObject.SetActive(true);
                travelState.SetDestinations();               
                break;

            case "questionWitnessState":
                DisableStates();
                witnessState.SetActive(true);
                SetWitnessButtons();
                break;

            case "questionWitness":
                gameState.selectedWitness = param;

                bool concludeGame = Global.instance.gameController.WitnessConcludesGame(param);               

                if (concludeGame || (Global.instance.gameState.timeRemaining -= Constants.witnessVisitCost) < 0)
                {
                    SceneManager.LoadScene("Endgame");
                } else
                {
                    SceneManager.LoadScene("Witness");
                }
  
                break;

            case "travelButton":
                TravelNode destination = null;
                foreach(TravelNode node in gameState.travelNodes)
                {
                    if (node.nodeName.Equals(param))
                    {
                        destination = node;
                        break;
                    }
                }

                Global.instance.gameController.TravelTo(destination);
                SetState("mainMenu");
                SetState("hideTerminal");
                break;

            case "exit":
                Debug.Log("Exit clicked");
                Application.Quit();
                break;

            case "dossier":
                DisableStates();
                dossierState.SetActive(true);
                break;

            case "dossierNext":
                dossierPagenator.IncrementPage();
                break;

            case "dossierPrev":
                dossierPagenator.DecrementPage();
                break;

            case "warrant":
                DisableStates();
                warrantState.SetActive(true);
                break;

            case "incrementWarrantTrait":
                warrantController.IncrementTrait(param);
                break;

            case "decrementWarrantTrait":
               warrantController.DecrementTrait(param);
                break;

            case "issueWarrant":
                warrantController.IssueWarrant();
                break;
        }

        if(!newState.Equals("showTerminal") && !newState.Equals("hideTerminal")) state = newState;

    }

    void SetWitnessButtons()
    {
        Location currentLocation = Global.instance.locations.GetLocation(gameState.currentNode.nodeName);
        if(currentLocation.witnesses == null)
        {
            Debug.Log("Current location witnesses null");
            return;
        }

        for (int i = 0; i < currentLocation.witnesses.Length; i++)
        {
            witnessButtons[i].GetComponent<TMP_Text>().text = "* " + currentLocation.witnesses[i].displayName;
            witnessButtons[i].GetComponent<TerminalStateButton>().param = currentLocation.witnesses[i].referenceName;
        }
    }

    void DisableStates()
    {
        newGameState.SetActive(false);
        newGameStateSmall.SetActive(false);
        briefingState.SetActive(false);
        briefingStateSmall.SetActive(false);
        mainMenuState.SetActive(false);
        mainMenuStateSmall.SetActive(false);
        travelStateObject.SetActive(false);
        witnessState.SetActive(false);
        dossierState.SetActive(false);
        warrantState.SetActive(false);
    }
}
