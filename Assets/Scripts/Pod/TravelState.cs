using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TravelState : MonoBehaviour
{
    public List<SpriteRenderer> destination0Renderers, destination1Renderers, destination2Renderers, destination3Renderers;
    List<List<SpriteRenderer>> spriteRenderers; //The renderers for each destionation are put in a list here so they can be iterated later (Unity can't show nested lists, or it'd be done that way in the inspector)

    public List<Sprite> timeSymbolsBright;

    public List<GameObject> destinationObjects;

    Dictionary<string, List<string>> randomisedTravelLocations;

    public void SetDestinations()
    {
        randomisedTravelLocations = Global.instance.gameState.randomisedTravelLocations; //Set this here due to lifecycle of this class

        if(spriteRenderers == null)
        {
            spriteRenderers = new List<List<SpriteRenderer>>();
            spriteRenderers.Add(destination0Renderers);
            spriteRenderers.Add(destination1Renderers);
            spriteRenderers.Add(destination2Renderers);
            spriteRenderers.Add(destination3Renderers);
        }

        string nodeName = Global.instance.gameState.currentNode.nodeName;
        

        //Turn all the destinations off first, then just turn them on as needed
        foreach (var destination in destinationObjects)
        {
            destination.SetActive(false);
        }

        if (!randomisedTravelLocations.ContainsKey(nodeName)) return; //Possible if visiting easter eggs

        for (int i = 0; i < randomisedTravelLocations[nodeName].Count; i++)
        {
            string destinationName = randomisedTravelLocations[nodeName][i];

            destinationObjects[i].SetActive(true);
            destinationObjects[i].GetComponentInChildren<TMP_Text>().text = Global.instance.locations.GetLocationDisplayName(destinationName);
            destinationObjects[i].GetComponent<TerminalStateButton>().param = destinationName;

            TimecodeController.SetTimecodeSprites(new List<int>(Global.instance.gameState.timeCodes[destinationName]), spriteRenderers[i], timeSymbolsBright);

          /*  string code = "Destination: " + randomisedTravelLocations[nodeName][i] + " code: ";
            foreach (int c in Global.instance.gameState.timeCodes[randomisedTravelLocations[nodeName][i]])
            {
                code += c + " ";
            }*/
           // Debug.Log(code);
        }
    }
}
