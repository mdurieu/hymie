using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimecodeController : MonoBehaviour
{
    public List<Sprite> timeSymbolsDark;   

    public List<SpriteRenderer> renderers;

    public TerminalController terminalController;

    GameState gameState;

    private void Start()
    {
        gameState = Global.instance.gameState;
        UpdateUI();
    }

    public void OnClick(int buttonId)
    {
        Global.instance.soundController.PlayOnce("buttonPress");

        if(buttonId == -1) //This is the enter button
        {
            if (Global.instance.gameState.travelNodes == null) return; //Don't let the player try to go anywhere before the game is initialised (like to the easter eggs)
            SubmitTimeCode();
            return;
        }

        if(gameState.timeCode.Count == 6)
        {
            gameState.timeCode.Clear();
        }

        gameState.timeCode.Add(buttonId);

        UpdateUI();
    }

    public void SetCodeToPodBay()
    {
        if (gameState == null) gameState = Global.instance.gameState;
        gameState.timeCode = Global.instance.gameState.timeCodes["podBay"].ToList();

        UpdateUI();
    }

    void SubmitTimeCode()
    {
        if(gameState.timeCode.Count != 6)
        {
            gameState.timeCode.Clear();
            UpdateUI();
            return;
        }

        string destination = "";

        foreach(var entry in Global.instance.gameState.timeCodes)
        {
            bool match = true;
            for(int i = 0; i < gameState.timeCode.Count; i++)
            {
                if (gameState.timeCode[i] != entry.Value[i]) match = false;
            }

            if (match)
            {
                destination = entry.Key;
                break;
            }

        }

        if (destination.Length == 0) {
            gameState.timeCode.Clear();
            UpdateUI();
            return;
         }

        if (destination.Equals(Global.instance.gameState.currentNode.nodeName)) return;

        if (destination.Equals("podBay"))
        {
            Global.instance.gameState.currentNode = Global.instance.gameState.travelNodes[0]; //This is the podBay, set it so the endgame knows the correct failure message
            SceneManager.LoadScene("Endgame");
            return;
        }

        terminalController.SetState("travelButton", destination);
    }

    void UpdateUI()
    {
        SetTimecodeSprites(gameState.timeCode, renderers, timeSymbolsDark);
    } 

    //This is a static method as it's called by both the timecode keypad and also in the terminal interface
    public static void SetTimecodeSprites(List<int> timeCode, List<SpriteRenderer> renderers, List<Sprite> timeSymbols)
    {
        List<int> tempCode = new List<int>(timeCode);
        for (int i = 5; i >= 0; i--) //Iterates over the code sprites, from right to left
        {
            if (tempCode.Count > 0)
            {
                int lastIndex = tempCode.Count - 1;
                renderers[i].enabled = true;
                renderers[i].sprite = timeSymbols[tempCode[lastIndex]];
                tempCode.RemoveAt(lastIndex);
            }
            else
            {
                renderers[i].enabled = false;
            }
        }
    }
}
