using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimecodeButton : MonoBehaviour
{
    TimecodeController timecodeController;
    public int buttonId;
    void Start()
    {
        timecodeController = GameObject.Find("TimecodeController").GetComponent<TimecodeController>();
    }

    private void OnMouseDown()
    {
        timecodeController.OnClick(buttonId);
    }
}
