using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class WarrantController : MonoBehaviour
{
    public TMP_Text sexText, speciesText, personalityText, hairText, featuresText, headerText, issueWarrantButtonText, noMatchFoundText;

    public BoxCollider2D issueWarrantCollider;        

    float noMatchFlashDuration = 0.75f;
    int noMatchFlashLimit = 8;
    float noMatchFlashTimer = 0;
    int noMatchFlashCount = 0;
    bool flashing = false;

    GameState gameState;


    private void Start()
    {
        gameState = Global.instance.gameState;

        UpdateUI();
    }

    private void Update()
    {
        if (flashing)
        {
            noMatchFlashTimer += Time.deltaTime;
           
            if(noMatchFlashTimer > noMatchFlashDuration)
            {               
                noMatchFlashCount++;
                noMatchFoundText.enabled = !noMatchFoundText.enabled;
                noMatchFlashTimer = 0;
            }

            if(noMatchFlashCount > noMatchFlashLimit)
            {
                flashing = false;
                noMatchFoundText.enabled = false;
            }
        }
    }

    public void IssueWarrant()
    {
        Dictionary<string, Villain> villains = Global.instance.villains.GetVillains(); //This returns a clone, not a reference so it's safe to manipulate

        foreach(var entry in gameState.warrantTraitDefinitions)
        {
            string selectedValue = entry.Value[gameState.selectedWarrantTraits[entry.Key]];

            if (selectedValue.Equals("---")) continue; 
            
            
            for(int i = villains.Count - 1; i >= 0; i--) //Iterate backwards over this collection so elements can be removed
            {
                foreach(Trait attribute in villains.ElementAt(i).Value.traits)
                {
                    if (attribute.name.Equals(entry.Key))
                    {
                        if (!attribute.value.Equals(selectedValue))
                        {
                            villains.Remove(villains.ElementAt(i).Key);
                        }
                    }
                }
            }
        }       

        if(villains.Count == 1)
        {
            gameState.warrantTarget = villains.ElementAt(0).Key;
            flashing = false;
            noMatchFoundText.enabled = false;
            Global.instance.soundController.PlayOnce("positiveChime");
        } else
        {
            flashing = true;
            noMatchFlashTimer = 0;
            noMatchFlashCount = 0;
            noMatchFoundText.enabled = true;
            Global.instance.soundController.PlayOnce("negativeChime");
        }

        UpdateUI();
    }

    public void IncrementTrait(string traitName)
    {
        gameState.selectedWarrantTraits[traitName]++;

        if (gameState.selectedWarrantTraits[traitName] >= gameState.warrantTraitDefinitions[traitName].Length) gameState.selectedWarrantTraits[traitName] = 0;
        UpdateUI();
    }

    public void DecrementTrait(string traitName)
    {
        gameState.selectedWarrantTraits[traitName]--;

        if (gameState.selectedWarrantTraits[traitName] < 0) gameState.selectedWarrantTraits[traitName] = gameState.warrantTraitDefinitions[traitName].Length - 1;
        UpdateUI();
    }

    void UpdateUI()
    {
        sexText.text = Utils.UpperFirst(gameState.warrantTraitDefinitions["sex"][gameState.selectedWarrantTraits["sex"]]);
        speciesText.text = Utils.UpperFirst(gameState.warrantTraitDefinitions["species"][gameState.selectedWarrantTraits["species"]]);
        personalityText.text = Utils.UpperFirst(gameState.warrantTraitDefinitions["personality"][gameState.selectedWarrantTraits["personality"]]);
        hairText.text = Utils.UpperFirst(gameState.warrantTraitDefinitions["hair"][gameState.selectedWarrantTraits["hair"]]);
        featuresText.text = Utils.UpperFirst(gameState.warrantTraitDefinitions["features"][gameState.selectedWarrantTraits["features"]]);

        if(gameState.warrantTarget.Length > 0)
        {
            headerText.text = "Warrant issed for: " + gameState.warrantTarget;
            issueWarrantCollider.enabled = false;
            issueWarrantButtonText.color = Constants.textColorDark;
        } else
        {
            headerText.text = "No Warrant Issued";
            issueWarrantCollider.enabled = true;
            issueWarrantButtonText.color = Constants.textColor;
        }
    }
}
