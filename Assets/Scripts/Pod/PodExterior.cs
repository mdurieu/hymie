using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PodExterior : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        SetSprite();
    }

    public void SetSprite()
    {
        if (Global.instance.podExteriorsCache.cachedSprite == null) return;
        spriteRenderer.sprite = Global.instance.podExteriorsCache.cachedSprite;  
    }
}
