using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLengthCollider : MonoBehaviour
{
    //TODO: This whole class can be removed and these buttons can have TerminalStateButton components
    // and add a new state in the SetState switch statement that takes the game length as a param

    public TerminalController terminalController;
    public string length;

    private void OnMouseDown()

    {
        switch (length)
        {
            case "short":
                terminalController.SelectGameDuration(Constants.shortGameLength, Constants.shortGameFailures);
                break;

            case "medium":
                terminalController.SelectGameDuration(Constants.medGameLength, Constants.medGameFailures);
                break;

            case "long":
                terminalController.SelectGameDuration(Constants.longGameLength, Constants.longGameFailures);
                break;
        }      
    }
}
