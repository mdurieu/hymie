using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SkipToEnd : MonoBehaviour
{
    private void OnMouseDown()
    {
        SceneManager.LoadScene("Endgame");
    }
}
