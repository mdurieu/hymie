using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalStateButton : MonoBehaviour
{
    public TerminalController terminalController;
    public string stateName;
    public string param;

    private void OnMouseDown()
    {
        terminalController.SetState(stateName, param);
    }
}
