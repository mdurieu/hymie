using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class DossierPagenator : MonoBehaviour
{
    int currentPage = 0;
    Dictionary<string, Villain> villains;
    public TMP_Text dossierText;
    

    private void Start()
    {
        villains = Global.instance.villains.GetVillains();
        SetPage();
    }

    public void SetPage()
    {
        Villain villain = villains.ElementAt(currentPage).Value;

        string text = "";
        
        text += "Name: " + Utils.UpperFirst(villain.name) + "\n\n";

        text += "Sex: " + Utils.UpperFirst(GetTraitByName("sex", villain)) + "\n\n";

        text += "Species: " + Utils.UpperFirst(GetTraitByName("species", villain)) + "\n\n";

        text += "Personality: " + Utils.UpperFirst(GetTraitByName("personality", villain)) + "\n\n";

        text += "Hair: " + Utils.UpperFirst(GetTraitByName("hair", villain)) + "\n\n";

        text += "Features: " + Utils.UpperFirst(GetTraitByName("features", villain)) + "\n\n";

        dossierText.text = text;
    }

    public void IncrementPage()
    {
        currentPage++;
        if (currentPage >= villains.Count) currentPage = 0;
        SetPage();
    }

    public void DecrementPage()
    {
        currentPage--;
        if (currentPage < 0) currentPage = villains.Count - 1;
        SetPage();
    }

    string GetTraitByName(string queryTrait, Villain villain)
    {
        foreach(Trait trait in villain.traits)
        {
            if (trait.name.Equals(queryTrait)) return trait.value;
        }

        return "Trait not found";
    }
    
}
