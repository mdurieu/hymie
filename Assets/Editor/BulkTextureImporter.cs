using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class BulkTextureImporter : EditorWindow
{

    [MenuItem("Tools/Import Textures")]
    static void ImportTextures()
    {
       BulkImport();
    }

    public static void BulkImport()
    {
        foreach (string file in Directory.EnumerateFiles(Application.dataPath + "/Textures", "*.*", SearchOption.AllDirectories))
        {
            if ((file.Contains(".png") || file.Contains(".bmp")) && !file.Contains(".meta"))
            {
                //Trim the file path down to start with the /Assets part
                int index = file.IndexOf("Assets");
                string trimmed = file.Substring(index);
                //Directory search gets paths with backslashes, need to convert all to forward slashes
                string replaced = trimmed.Replace("\\", "/");
                
                ImportTexture(replaced);
            }
           
        }
    }

    public static Texture2D ImportTexture(string path)
    {
        TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

        if (textureImporter == null) return null;

        textureImporter.textureType = TextureImporterType.Sprite;
        textureImporter.spriteImportMode = SpriteImportMode.Single;
        textureImporter.mipmapEnabled = false;
        textureImporter.spritePixelsPerUnit = 1;
        textureImporter.filterMode = FilterMode.Point;
        textureImporter.isReadable = true;
        textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
        AssetDatabase.ImportAsset(path);

        //Reload the texture after it's been imported with proper params
        Texture2D texture = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;

        return texture;
    }
}
