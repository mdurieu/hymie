# Where In Space Is Hymie Lipschitz (And Who Really Cares)

This game is an imagining of the satirical title [Where In The World Is Hymie Lipschitz? (And Who Really Cares)](https://spacequest.fandom.com/wiki/Where_In_The_World_Is_Hymie_Lipschitz%3F_(And_Who_Really_Cares)) which appears in the adventure game Space Quest IV published in 1991 by Sierra On-Line. Being a spoof of the Carmen Sandiego series, this game takes the same general formula and sets it in the Space Quest universe.

The purpose of this project, aside from just a bit of fun, is to allow junior developers (or anyone interested) to have access to a project which they can contribute to or expand upon, and also serve as a reference for implementation of several features of the Unity engine. This document itself (at the time of writing) contains the basics of the game structure, but is free to be elaborated upon by contributors.

The game was also developed to serve as an event in the Space Quest Olympics, which takes place over on the [Space Quest Historian Discord](https://discord.gg/9mcCQt2).

If you're new to development, then a good place to start is loading up the project in Unity and just following through progression of a game step by step, find places where more explanation is needed, and submit updates to this readme. This familiarisation process is also an excellent place to identify places where the code is unclear/bloated/inconsistent and could use refactoring (there are many such places) Many functions also need comments describing their purpose and these will become particularly evident to fresh eyes learning the project.

## Unity Version and Setup
Development requires Unity version 2020.3.32f1

Clone this repository to a location of your choice, then in the Unity Hub select "Open" from the projects menu. Unity should import the project and generate the local files it needs (which the .gitignore file should omit from any commits you make). Any changes to the game should be confined to the folders already included in the repository.

## Scene structure
The game consists of 4 scenes - Intro, Main, Witness, and Endgame

**Intro**
This is an isolated scene which is first in the build order (and thus the game loads into first). It can be expanded on in any of number of ways in the future but must remain self-contained (ie do not add anything that uses DontDestroyOnLoad etc), and load the Main scene on completion.

**Main**
The main scene loads the player into the initial Podbay view, with access to the timepod terminal computer to set conditions for starting a new game. Also key to this scene is that it initialises the Global GameObject singleton which contains many persistent references such as character definitions, music and sound controllers and the gamestate. **When developing, run the game from this scene.** (The game can also be run from Intro scene but that will just waste time if not working on Intro).


**Witness**
The witness scene is loaded when the player chooses to question a witness at any location. This is a rather lightweight scene with almost all functionality controlled by the WitnessController class.

**Endgame**
The Endgame scene is triggered when the player either apprehends the criminal, or reaches any of the failure states. It displays a message relative to the context of the win/failure and the option to move to the next case, or restart a new game.

## Application Flow
The game begins loading into the Main scene. This will initialise the singleton **Global** object which contains persistent references and game state information. Familiarise yourself with this object and its components, it is referred to requently from many places in the game. Some components to note about it are:
 - Strings: All strings are loaded in from JSON reference files in the Assets/Resources/Strings folder. This class provides access to them via their reference name.
 
 - Game Element Definitions: Various elements of the game are also described in JSON files, namely the Stolen Objects, Villains, and Locations. These are parsed on launch and referenced at various points during gameplay. Definitions are located in the Assets/Resources folder

- Asset caches: Several sound and graphical resources are loaded asynchronously using Unity's **Addressables** system. This is done for performance reasons. An example is in the TravelTo method in GameController.cs, when a new destination is selected, the song for the destination, as well as the Time Pod exterior sprite, and witness backgrounds/portaits are loaded while the time travel animation is playing.

- GameState: The GameState object holds all the information about the current game in progress, such as the definition of the travel network, successes and failures, and warrant information. It also contains the logic for generating the time codes, as well as a new game.

- GameController: Contains several methods for high-level progression through the game
- Music and Sound: Separate music and sound controller objects are children of the global object (as each has its own Audio Source component). They serve as a single point for sound or music to be controlled from anywhere in the game.

Once the Main scene has loaded, the majority of player interaction is done via the terminal and controlled by the TerminalController class. This has its own object in the scene root with references to both the pop-up "large" interactive terminal and the "small" terminal seen on the timepod dash. Most functions are carried out through the SetState method, which is called largely by buttons on the terminal which have the TerminalStateButton component attached (these components can then have their parameters easily set in the inspector).

The time code is controlled by the TimecodeController root object which has the TimecodeController script attached. This tracks the button presses, updates the UI and initiates travel if conditions are met.


**Witnesses**
When the player selects to question a witness from the terminal, the Witness scene will load. This is a very lightweight scene which just sets the portrait, background, and hint text from references in children of the Global object. The only interactivity is for the player to read the hint, then return to the pod.












